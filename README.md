# 2022 MIC Rstudio Singularity Image

Singularity image for 2022 MIC Course
Based on https://hub.docker.com/r/ibiem/docker_rstudio_ibiem2020

## Notes

- Because the download link for Cellranger has an expiration time of 30 min, a new link needs to be acquired [here](https://support.10xgenomics.com/single-cell-gene-expression/software/overview/welcome) for the container to be re-built successfully.

Pull with the following, replacing **TAG_NAME** with the image tag you want to pull:
```
singularity pull --force --dir /opt/apps/community/od_chsi_rstudio oras://gitlab-registry.oit.duke.edu/mic-course/2022-mic-r-studio-singularity-image:TAG_NAME
```
