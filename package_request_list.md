# R 
## CRAN 
- [x] remotes
- [x] scSorter
- [ ] tidyverse

## Bioconductor
- [x] plyranges
- [x] rtracklayer
- [x] monocle
- [ ] org.Mm.eg.db

## Other
- [x] Monocle 3 <https://cole-trapnell-lab.github.io/monocle3/>
    - Use 'install_github' from remotes package installed above

# Packages that were used in the paper 
    - https://doi.org/10.1016/j.celrep.2021.109118
 - [x] MiXCR
 - [x] scanpy
 - [x] ggpubr
 - [x] SCENIC
 - [x] fastqc
 - [x] fastp
 - [x] GENIE3
 - [x] RcisTarget
 - [x] AUCell
 - [x] clusterProfiler
 - [x] tcr
 - [x] immunarch
 - [x] STAR
 - [x] DESeq2

